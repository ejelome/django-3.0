from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.urls import reverse
from django.views import generic

from .models import Choice, Question


class IndexView(generic.ListView):
    context_object_name = 'questions'

    def get_queryset(self):
        today = timezone.now()
        return (
            Question
            .objects
            .filter(pub_date__lte=today)
            .order_by('-pub_date')

        )[:5]


class DetailView(generic.DetailView):
    model = Question

    def get_queryset(self):
        today = timezone.now()
        return Question.objects.filter(pub_date__lte=today)


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/question_results.html'


def vote(request, pk):
    question = get_object_or_404(Question, pk=pk)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        template = 'polls/question_detail.html'
        context = {
            'question': question,
            'error_message': 'You didn\'t select a choice.'
        }
        return render(request, template, context)
    else:
        selected_choice.votes += 1
        selected_choice.save()

    return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
