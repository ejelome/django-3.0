import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from .models import Question

class QuestionModelTests(TestCase):
    def test_was_published_recently_with_future_question(self):
        tomorrow = timezone.now() + datetime.timedelta(days=1)
        future_question = Question(pub_date=tomorrow)
        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        past_today = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=past_today)
        self.assertIs(recent_question.was_published_recently(), True)

    def test_was_published_recently_with_old_question(self):
        yesterday = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=yesterday)
        self.assertIs(old_question.was_published_recently(), False)


def create_question(question_text, days):
    past_today = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=past_today)


class QuestionIndexViewTests(TestCase):
    def test_no_questions(self):
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'No polls are available.')
        self.assertQuerysetEqual(response.context['questions'], [])

    def test_past_question(self):
        create_question(question_text='Past question.', days=-1)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['questions'],
            ['<Question: Past question.>']
        )

    def test_future_question(self):
        create_question(question_text='Future question.', days=1)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, 'No polls are available')
        self.assertQuerysetEqual(response.context['questions'], [])

    def test_display_only_past_questions(self):
        create_question(question_text='Past question.', days=-1)
        create_question(question_text='Future question.', days=1)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['questions'],
            ['<Question: Past question.>']
        )

    def test_display_multiple_questions(self):
        create_question(question_text='Past question.', days=-1)
        create_question(question_text='Older question.', days=-2)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['questions'],
            [
                '<Question: Past question.>',
                '<Question: Older question.>'
            ]
        )


class QuestionDetailViewTests(TestCase):
    def test_future_question(self):
        future_question = create_question(question_text='Future question.', days=1)
        url = reverse('polls:detail', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        past_question = create_question(question_text='Past question.', days=-1)
        url = reverse('polls:detail', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)
