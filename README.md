django-3.0
==========

Django Polls app in 3.0

-------------------------------------------------------------------------------

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [django-3.0](#django-30)
    - [Prerequisites](#prerequisites)
    - [Setup](#setup)
    - [Polls app structure](#polls-app-structure)
    - [Shell commands](#shell-commands)
    - [Structure](#structure)
        - [Project](#project)
        - [App](#app)
        - [Django Admin template](#django-admin-template)
    - [References](#references)
    - [License](#license)

<!-- markdown-toc end -->

-------------------------------------------------------------------------------

Prerequisites
-------------

- [Pipenv](https://github.com/pypa/pipenv)

-------------------------------------------------------------------------------

Setup
-----

1. Activate virtual environment:

   ``` shell
   $ pipenv shell
   ```

2. Install project:

   ``` shell
   $ \
   pipenv install --dev            # install dependencies
   cd project                      # change to dir
   ./manage.py migrate             # apply (modified) database schema
   ./manage.py test                # run test cases
   ./manage.py loaddata user polls # load initial data
   ./manage.py runserver           # run web server
   ```

3. Log in at <http://localhost:8000/admin> with:

   - Username: `admin`
   - Password: `foobar`

4. The visit <http://localhost:8000/polls>

-------------------------------------------------------------------------------

Polls app structure
-------------------

``` shell
<project>/
├── <app>/
│   ├── fixtures/
│   │   └── <app>.json
│   ├── migrations/
│   │   ├── 0001_initial.py
│   │   └── __init__.py
│   ├── static/
│   │   └── <app>/
│   │       ├── css/
│   │       │   └── <app>.css
│   │       ├── img/
│   │       │   └── <app>.png
│   │       └── js/
│   │           └── <app>.js
│   ├── templates/
│   │   └── <app>/
│   │       ├── base.html
│   │       ├── question_detail.html
│   │       ├── question_list.html
│   │       └── question_results.html
│   ├── admin.py
│   ├── apps.py
│   ├── __init__.py
│   ├── models.py
│   ├── tests.py
│   ├── urls.py
│   └── views.py
├── <project>/
│   ├── asgi.py
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── fixtures/
│   └── <model>.json
├── static/
│   ├── css/
│   │   └── <project>.css
│   ├── img/
│   │   └── <project>.png
│   └── js/
│       └── <project>.js
├── templates/
│   ├── admin/
│   │   └── base_site.html
│   ├── base.html
│   ├── footer.html
│   └── header.html
└── manage.py
```

-------------------------------------------------------------------------------

Shell commands
--------------

| Command                       | Description                      |
| :---------------------------- | :------------------------------- |
| `django-admin startproject`   | Create a project                 |
| `./manage.py startapp`        | Create an app                    |
| `./manage.py makemigrations`  | Create migration                 |
| `./manage.py migrate`         | Apply (modified) database schema |
| `./manage.py sqlmigrate`      | Show migration in SQL format     |
| `./manage.py test`            | Run test cases                   |
| `./manage.py loaddata`        | Load initial data                |
| `./manage.py shell`           | Open Django shell                |
| `./manage.py runserver`       | Run Django web server            |
| `./manage.py createsuperuser` | Create Django superuser          |

-------------------------------------------------------------------------------

Structure
---------

**Note:** The follow directories and files should be created manually:

- `<app>`'s `urls.py`
- `fixtures/*.{json,yml}`
- `static/`
- `templates/`

### Project ###

``` shell
<project>/           # project root (name doesn't matter)
├── manage.py        # CLI utility (to interact with the project)
└── <project>/       # the actual package of the project (used on imports)
    ├── asgi.py      # a replacement for wsgi for asynchronous-capable web servers
    ├── __init__.py  # empty file to make the directory a package
    ├── settings.py  # project configuration file
    ├── urls.py      # main URLs of the project
    └── wsgi.py      # default web server to run the project
```

### App ###

``` shell
<app>/              # the app root
├── admin.py        # display the app model in Django Admin
├── apps.py         # app configuration file
├── __init__.py     # empty file to make the directory a package
├── migrations/     # app migration files to alter database table
│   └── __init__.py # empty file to make the directory a package
├── models.py       # app model (a class to map the database table)
├── tests.py        # app tests
└── views.py        # app controller
```

### Django Admin template ###

``` shell
$ $(pipenv --venv)/lib/python3.7/site-packages/django/contrib/admin/templates/admin/
```

-------------------------------------------------------------------------------

References
----------

- [Getting started](https://docs.djangoproject.com/en/3.0/intro)

-------------------------------------------------------------------------------

License
-------

`django-3.0` is licensed under [MIT](./LICENSE).
